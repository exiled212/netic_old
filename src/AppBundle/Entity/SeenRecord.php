<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SeenRecord
 * @ORM\Entity(repositoryClass="SeenRecordRepository")
 * @ORM\Table(name="seen_records")
 */
class SeenRecord
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * Many seen records have one user
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="Comunication",inversedBy="seenBy")
     * @ORM\JoinColumn(name="comunication_id", referencedColumnName="id")
     */
    protected $comunication;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return SeenRecord
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return SeenRecord
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get comunication
     *
     * @return \AppBundle\Entity\Comunication
     */
    public function getComunication()
    {
        return $this->comunication;
    }

    /**
     * Set comunication
     *
     * @param \AppBundle\Entity\Comunication $comunication
     *
     * @return SeenRecord
     */
    public function setComunication(\AppBundle\Entity\Comunication $comunication = null)
    {
        $this->comunication = $comunication;

        return $this;
    }
}
