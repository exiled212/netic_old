<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;





/**
 * Class GlobalSettings
 * @package AppBundle\Entity
 * @ORM\Entity()
 *
 */

class GlobalSettings{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;
    /**
     * @ORM\Column(type="boolean")
     */
    private $boss_messaging = true;
    /**
     * @ORM\Column(type="boolean")
     */
    private $employee_messaging = true;
    /**
     * @ORM\Column(type="boolean")
     */
    private $admin_messaging = true;


    /**
     * @ORM\Column(type="boolean")
     */
    private $manager_to_group_messaging = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $manager_direct_messages = true;

    /**
     * Get bossMessaging
     *
     * @return boolean
     */
    public function getBossMessaging()
    {
        return $this->boss_messaging;
    }

    /**
     * Set bossMessaging
     *
     * @param boolean $bossMessaging
     *
     * @return GlobalSettings
     */
    public function setBossMessaging($bossMessaging)
    {
        $this->boss_messaging = $bossMessaging;

        return $this;
    }

    /**
     * Get employeeMessaging
     *
     * @return boolean
     */
    public function getEmployeeMessaging()
    {
        return $this->employee_messaging;
    }

    /**
     * Set employeeMessaging
     *
     * @param boolean $employeeMessaging
     *
     * @return GlobalSettings
     */
    public function setEmployeeMessaging($employeeMessaging)
    {
        $this->employee_messaging = $employeeMessaging;

        return $this;
    }

    /**
     * Get adminMessaging
     *
     * @return boolean
     */
    public function getAdminMessaging()
    {
        return $this->admin_messaging;
    }

    /**
     * Set adminMessaging
     *
     * @param boolean $adminMessaging
     *
     * @return GlobalSettings
     */
    public function setAdminMessaging($adminMessaging)
    {
        $this->admin_messaging = $adminMessaging;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get managerToGroupMessaging
     *
     * @return boolean
     */
    public function getManagerToGroupMessaging()
    {
        return $this->manager_to_group_messaging;
    }

    /**
     * Set managerToGroupMessaging
     *
     * @param boolean $managerToGroupMessaging
     *
     * @return GlobalSettings
     */
    public function setManagerToGroupMessaging($managerToGroupMessaging)
    {
        $this->manager_to_group_messaging = $managerToGroupMessaging;

        return $this;
    }

    /**
     * Get managerDirectMessages
     *
     * @return boolean
     */
    public function getManagerDirectMessages()
    {
        return $this->manager_direct_messages;
    }

    /**
     * Set managerDirectMessages
     *
     * @param boolean $managerDirectMessages
     *
     * @return GlobalSettings
     */
    public function setManagerDirectMessages($managerDirectMessages)
    {
        $this->manager_direct_messages = $managerDirectMessages;

        return $this;
    }
}
