<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ComunicationRepository")
 * @ORM\Table(name="comunication")
 */
class Comunication {


    const TYPE_GENERAL = 1;
    const TYPE_GROUP =  2;
    const TYPE_DIRECT = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;
    /**
     * @ORM\Column(type="datetime")
     */
    protected  $date;


    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      min = 4,
     *      max = 50,
     *      minMessage = "El titulo debe tener al menos  {{ limit }} characteres",
     *      maxMessage = "El titulo no puede tener mas de {{ limit }} characteres"
     * )
     * @Assert\NotBlank()
     */
    protected  $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      min = 10,
     *      max = 20000,
     *      minMessage = "El contenido debe tener al menos  {{ limit }} characteres",
     *      maxMessage = "El contenido no puede tener más de {{ limit }} characteres"
     * )
     * @Assert\NotBlank()
     */
    protected  $content;

    //address data
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\Type(type="AppBundle\Entity\User")
     * @Assert\Valid()
     */
    protected $author;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $acceptsReplies = false;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     * @Assert\Type(type="AppBundle\Entity\Department")
     */
    private $department;
    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="users_comunicates",
     *      joinColumns={@ORM\JoinColumn(name="comunicate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $users;
    /**
     * @ORM\Column(type="integer")
     * @Assert\Choice(
     *     choices = {Comunication::TYPE_GENERAL,
     *                Comunication::TYPE_GROUP,
     *                Comunication::TYPE_DIRECT})
     * @Assert\NotBlank()
     */

    private $type = Comunication::TYPE_GENERAL;
    /**
     * Many Comunicates  have Many Attachements.
     * @ORM\ManyToMany(targetEntity="Attachment",cascade={"persist"})
     * @ORM\JoinTable(name="comunication_attachments",
     *      joinColumns={@ORM\JoinColumn(name="comunication_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="attachment_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $attachments;

    /**
     * @ORM\OneToMany(targetEntity="ComunicationReply", mappedBy="comunication", cascade={"remove"})
     */
    private $replies;


    /**
     * @var Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\View", mappedBy="message", cascade={"remove"}, orphanRemoval=true)
     */

    private $views;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->views = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Comunication
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Comunication
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return Comunication
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Comunication
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get workgroup
     *
     * @return \AppBundle\Entity\Department
     */
    public function getWorkgroup()
    {
        return $this->department;
    }

    /**
     * Set workgroup
     *
     * @param \AppBundle\Entity\Department $workgroup
     *
     * @return Comunication
     */
    public function setWorkgroup(\AppBundle\Entity\Department $workgroup = null)
    {
        $this->department = $workgroup;

        return $this;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Comunication
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Comunication
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get acceptsReplies
     *
     * @return boolean
     */
    public function getAcceptsReplies()
    {
        return $this->acceptsReplies;
    }

    /**
     * Set acceptsReplies
     *
     * @param boolean $acceptsReplies
     *
     * @return Comunication
     */
    public function setAcceptsReplies($acceptsReplies)
    {
        $this->acceptsReplies = $acceptsReplies;

        return $this;
    }

    /**
     * Add attachement
     *
     * @param \AppBundle\Entity\Attachment $attachement
     *
     * @return Comunication
     */
    public function addAttachment(\AppBundle\Entity\Attachment $attachment)
    {


        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Remove attachement
     *
     * @param \AppBundle\Entity\Attachment $attachement
     */
    public function removeAttachment(\AppBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add reply
     *
     * @param \AppBundle\Entity\ComunicationReply $reply
     *
     * @return Comunication
     */
    public function addReply(\AppBundle\Entity\ComunicationReply $reply)
    {
        $this->replies[] = $reply;

        return $this;
    }

    /**
     * Remove reply
     *
     * @param \AppBundle\Entity\ComunicationReply $reply
     */
    public function removeReply(\AppBundle\Entity\ComunicationReply $reply)
    {
        $this->replies->removeElement($reply);
    }

    /**
     * Get replies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReplies()
    {
        return $this->replies;
    }


    /**
     * Add seenBy
     *
     * @param \AppBundle\Entity\User $seenBy
     *
     * @return boolean
     */
    public function addView(\AppBundle\Entity\User $user)
    {

        if ($this->views->contains($user)) {
            return false;
        } else {
            $this->views->add($user);
            return true;
        }
    }

    /**
     * Remove seenBy
     *
     * @param \AppBundle\Entity\User $seenBy
     */
    public function removeView(\AppBundle\Entity\User $seenBy)
    {
        $this->views->removeElement($seenBy);
    }

    /**
     * Get seenBy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViews()
    {
        return $this->views;
    }
}