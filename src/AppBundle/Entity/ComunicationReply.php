<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * ComunicationReply
 *
 * @ORM\Table(name="comunication_reply")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ComunicationReplyRepository")
 */
class ComunicationReply
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="Comunication", inversedBy="replies")
     * @ORM\JoinColumn(name="comunication_id", referencedColumnName="id")
     */

    private $comunication;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_been_seen", type="boolean")
     */
    private $hasBeenSeen = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set author
     *
     * @param \stdClass $author
     *
     * @return ComunicationReply
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ComunicationReply
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ComunicationReply
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get hasBeenSeen
     *
     * @return bool
     */
    public function getHasBeenSeen()
    {
        return $this->hasBeenSeen;
    }

    /**
     * Set hasBeenSeen
     *
     * @param boolean $hasBeenSeen
     *
     * @return ComunicationReply
     */
    public function setHasBeenSeen($hasBeenSeen)
    {
        $this->hasBeenSeen = $hasBeenSeen;

        return $this;
    }

    /**
     * Get comunication
     *
     * @return \AppBundle\Entity\Comunication
     */
    public function getComunication()
    {
        return $this->comunication;
    }

    /**
     * Set comunication
     *
     * @param \AppBundle\Entity\Comunication $comunication
     *
     * @return ComunicationReply
     */
    public function setComunication(\AppBundle\Entity\Comunication $comunication = null)
    {
        $this->comunication = $comunication;

        return $this;
    }
}
