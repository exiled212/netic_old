<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class RegistrationSuccessEventListener implements EventSubscriberInterface
{


    private $router;
    private $em;

    public function __construct(UrlGeneratorInterface $router, EntityManager $em)
    {

        $this->router = $router;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        //this will be called before

        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => ['onUserRegistrationSuccess', 2],
        );

    }

    /**
     * @param FormEvent $event
     * When the user registration is completed redirect
     * to the employee list page and avoid the automatic
     * mail sending and user authentication that happends
     *
     */
    public function onUserRegistrationSuccess(FormEvent $event)
    {


        $form = $event->getForm();
        /** @var User $user */
        $user = $form->getData();
        if ($user->getAdminRights()) {
            $user->addRole("ROLE_ADMIN");

        }
        if ($user->getGroupBoss()) {
            $user->addRole("ROLE_MANAGER");
            $department = $user->getDepartment();
            $department->setManager($user);
            $this->em->persist($department);
            $this->em->flush();
        }

        $user->setEnabled(true);

        $url = $this->router->generate('employees_list');

        $event->setResponse(new RedirectResponse($url));

    }


}