<?php


namespace AppBundle\EventListener;


use AppBundle\Entity\User;
use AppBundle\Services\FileUploadService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AvatarUploadListener{


    private  $uploader;

    public function __construct(FileUploadService $uploadService){


        $this->uploader = $uploadService;
    }

    public  function prePersist(LifecycleEventArgs $args){

            $entity = $args->getEntity();
            $this->uploadFile($entity);

    }
    public function  preUpdate(LifecycleEventArgs $args){

        $entity = $args->getEntity();
        $this->uploadFile($entity);

    }

    private function uploadFile($entity)
    {
        if($entity instanceof  User){
            $file = $entity->getAvatar();
            if($file instanceof UploadedFile){
                $fileName = $this->uploader->upload($file);
                $entity->setAvatar($fileName);
            }else if ($file == null){
                $entity->setAvatar("dummy.png");
            }

        }
    }


};




