<?php

namespace AppBundle\Api\Controller;


use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use AppBundle\Form\EmployeeType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class TestController extends Controller
{
    /**
     * @Route("/push/windows",name="api_push_windows")
     */
    public function sendMessage(){
        $this->get('')->publish(["Hello"]);
        return new Response("Ok");
    }

}