<?php


namespace AppBundle\Api\Controller;


use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use AppBundle\Form\EmployeeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserRestController extends RestController
{



    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/user/authenticate", name="api_user_authenticate")
     * @Method("POST")
     */

    public function loginAction(Request $request)
    {

        $data = json_decode($request->getContent(), true);

        $username = trim($data["user"]);
        $pass = $data["pass"];
        /** @var User $user */
        $user = $this->get("fos_user.user_manager")->findUserByUsername($username);
        if ($user) {
            if ($this->isValidPassword($user, $pass)) {
                //serialize user

                $user_array = $this->serializeUser($user);

                return new JsonResponse(["ok" => true, "data" => $user_array]);
            } else {
                return new JsonResponse(["ok" => false, "error" => "bad credentials"]);
            }
        } else {

            throw new NotFoundHttpException("invalid username " + $username);

        }
    }

    private function isValidPassword($user, $password)
    {

        $factory = $this->get('security.encoder_factory');

        $encoder = $factory->getEncoder($user);
        $salt = $user->getSalt();
        return $encoder->isPasswordValid($user->getPassword(), $password, $salt);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/user/load", name="api_user_load")
     * @Method("GET")
     */
    public function loadAction(Request $request)
    {

        $userId = $request->get("id", -1);
        if ($userId != -1) {
            $user = $this->get("fos_user.user_manager")->findUserBy(["id" => $userId]);
            return new JsonResponse($this->serializeUser($user));

        } else {
            throw new NotFoundHttpException("user not found");
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/user/update", name="api_user_update")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {


        $data = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository(User::class)->find($data["id"]);
        $form = $this->createForm(new EmployeeType(), $user);
        $form->submit($data);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return new JsonResponse(["ok" => true, "user" => $this->serializeUser($user)]);


    }

    /**
     * @Route("workgroup/add", name="api_workgroup_add_user")
     */
    public function addUserToDepartment(Request $request)
    {

        $data = json_decode($request->getContent(), true);
        $deparmentId = $data["id"];
        $usersId = $data["users"];
        $deparment = $this->getDoctrine()->getRepository(Department::class)->find($deparmentId);
        if($deparment == null){
          return new NotFoundHttpException("Department not found");
        }
        $em = $this->getDoctrine()->getManager();
        $userArr = [];
        foreach ($usersId as $userId) {
            /** @var User $user */
            $user = $this->getUserById($userId);

            if($user->isGroupBoss()){

                // if the user is the manager of one department
                // and is been moved to another, then
                // it stops been the manager of that department
                $oldDepartment = $user->getDepartment();
                $oldDepartment->setManager(null);
                $em->persist($oldDepartment);
            }
            $user->setDepartment($deparment);
            $em->persist($user);
            $userArr [] = $this->serializeUser($user);

        }
        $em->flush();
        return new JsonResponse(["ok" => true, "users" => $userArr], 201);
    }

    private function getUserById($user_id)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->find(intval($user_id));

        return $user;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/user/reset", name="api_user_reset")
     * @Method("POST")
     */

    public function changePassword(Request $request)
    {

        $data = json_decode($request->getContent(), true);

        $user_id = $data["user"];
        $old_pass = $data["pass"];

        $new_pass = $data["new_pass"];
        /** @var User $user */
        $user = $this->getUserById($user_id);

        if ($this->isValidPassword($user, $old_pass)) {
            $user->setPlainPassword($new_pass);
            $this->get("fos_user.user_manager")->updateUser($user);
            return new JsonResponse(["ok" => true, "data"=> $this->serializeUser($user)]);
        } else {
            return new JsonResponse(["ok" => false, "message" => "invalid password"]);
        }


    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/user/token", name="api_user_token")
     * @Method("POST")
     */
    public function setDeviceToken(Request $request)
    {

        $data = json_decode($request->getContent(), true);

        $user_id = intval($data["user"]);
        $token = $data["token"];
        $deviceType = intval($data["device_type"]);
        /** @var User $user */
        $user = $this->getUserById($user_id);
        $user->setDeviceToken($token);
        $user->setDeviceType($deviceType);
        $this->get("logger")->critical("Setting device token for user ". $user->getUsername());
        $this->get("logger")->critical("Setting device token value". $user->getDeviceToken());
        $this->get("logger")->critical("Setting device type value". $user->getDeviceType());
        /*

        switch ($dev_type) {
            case User::DEVICE_IOS:
                $user->setDeviceType(User::DEVICE_IOS);
                break;
            case User::DEVICE_ANDROID:
                $user->setDeviceType(User::DEVICE_WP);
                break;
            case User::DEVICE_WP:
                $user->setDeviceType(User::DEVICE_WP);
                break;
            case  User::DEVICE_WEB:
                $user->setDeviceType(User::DEVICE_WEB);
            default:
                throw new InvalidArgumentException("Unknown device type ".$device_type);
        }
        */
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($user);
        $em->flush();
        return new JsonResponse(["ok" => 1, 'user' => $this->serializeUser($user)]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/user/list", name="api_user_list")
     * @Method("GET")
     */

    public function getUserListAction()
    {

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $users_arr = [];
        foreach ($users as $user) {

            $user_arr [] = $this->serializeUser($user);
        }
        return new JsonResponse($user_arr);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/group/list", name="api_group_list")
     * @Method("GET")
     */

    public function getGroupListAction()
    {


        $groups = $this->getDoctrine()->getRepository(Department::class)->findAll();
        $groups_arr = [];
        foreach ($groups as $group) {

            $groups_arr [] = $this->serializeGroup($group);
        }
        return new JsonResponse($groups_arr);
    }

    /**
     * @Route("/user/avatar/upload",name="api_user_avatar_upload")
     * @Method("POST")
     */
    public function uploadAvatar(Request $request)
    {
        $id = $request->request->get("id");

        $user = $this->getUserById($id);

        $avatarFile = $request->files->get("avatar");

        if($avatarFile->isValid()){

            $avatar = $this->get("avatar_uploader")->upload($avatarFile);
            $user->setAvatar($avatar);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return new JsonResponse(["ok" => true, "data" => $this->serializeUser($user)], 201);


        }else{
            dump($avatarFile->getErrorMessage());
            die();

            return new NotFoundHttpException($avatarFile->getErrorMessage());
        }

    }


}
