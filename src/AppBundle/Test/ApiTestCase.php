<?php

namespace AppBundle\Test;

use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use Knp\Component\Pager\Event\BeforeEvent;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Exception;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DomCrawler\Crawler;



class ApiTestCase extends KernelTestCase
{

    private static $staticClient;
    /** @var  Client */
    protected $client;

    private $responseAsserter;

    /**
     * @var FormatterHelper
     */
    private $formatterHelper;


    /**
     * @var ConsoleOutput
     */
    private $output;
    private static $handler;

    private static $history;

    public static function setUpBeforeClass()
    {



        self::$handler = HandlerStack::create();

        self::$handler->push(Middleware::mapRequest(function(RequestInterface $request){
            $path =  $request->getUri();

            if (strpos($path, '/api') === 0) {

               $request->withUri('/app_test.php'.$path);

            }
            return $request;
        }));

        self::$staticClient = new \GuzzleHttp\Client([
            'base_uri' => 'http://127.0.0.1:8000',
            'http_errors' => false,
            "handler" => self::$handler
        ]);

        self::bootKernel();

    }


    protected function  asserter(){
        if($this->responseAsserter === null){
            $this->responseAsserter = new ResponseAsserter();

        }
        return $this->responseAsserter;
    }
    protected function setUp()
    {
        $this->client = self::$staticClient;

        self::$kernel->boot();
    }

    protected function getAuthorizedHeaders($username, $headers = array())
    {
        $token = $this->getService('lexik_jwt_authentication.encoder')
            ->encode(['username' => $username]);
        $headers['Authorization'] = 'Bearer ' . $token;

        return $headers;
    }

    protected function getService($id)
    {
        $container = self::$kernel->getContainer();

        return $container->get($id);
    }


    protected function getEntityManager()
    {
        return $this->getService('doctrine.orm.entity_manager');
    }

//    protected function onNotSuccessfulTest(Exception $e)
//    {
////        if (self::$history && $lastResponse = self::$history->get()) {
////            $this->printDebug('');
////            $this->printDebug('<error>Failure!</error> when making the following request:');
////            $this->printLastRequestUrl();
////            $this->printDebug('');
////            $this->debugResponse($lastResponse);
////        }
////        throw $e;
//    }
    protected function printLastRequestUrl()
    {
        $lastRequest = self::$history->getLastRequest();
        if ($lastRequest) {
            $this->printDebug(sprintf('<comment>%s</comment>: <info>%s</info>', $lastRequest->getMethod(), $lastRequest->getUrl()));
        } else {
            $this->printDebug('No request was made.');
        }
    }


    /**
     * Print a message out - useful for debugging
     *
     * @param $string
     */
    protected function printDebug($string)
    {
        if ($this->output === null) {
            $this->output = new ConsoleOutput();
        }
        $this->output->writeln($string);
    }


    protected function debugResponse(ResponseInterface $response)
    {

//        $this->printDebug(AbstractMessage::getStartLineAndHeaders($response));
        $body = (string) $response->getBody();
        $contentType = $response->getHeader('Content-Type');
        if ($contentType == 'application/json' || strpos($contentType, '+json') !== false) {
            $data = json_decode($body);
            if ($data === null) {
                // invalid JSON!
                $this->printDebug($body);
            } else {
                // valid JSON, print it pretty
                $this->printDebug(json_encode($data, JSON_PRETTY_PRINT));
            }
        } else {
            // the response is HTML - see if we should print all of it or some of it
            $isValidHtml = strpos($body, '</body>') !== false;
            if ($isValidHtml) {
                $this->printDebug('');
                $crawler = new Crawler($body);
                // very specific to Symfony's error page
                $isError = $crawler->filter('#traces-0')->count() > 0
                    || strpos($body, 'looks like something went wrong') !== false;
                if ($isError) {
                    $this->printDebug('There was an Error!!!!');
                    $this->printDebug('');
                } else {
                    $this->printDebug('HTML Summary (h1 and h2):');
                }
                // finds the h1 and h2 tags and prints them only
                foreach ($crawler->filter('h1, h2')->extract(array('_text')) as $header) {
                    // avoid these meaningless headers
                    if (strpos($header, 'Stack Trace') !== false) {
                        continue;
                    }
                    if (strpos($header, 'Logs') !== false) {
                        continue;
                    }
                    // remove line breaks so the message looks nice
                    $header = str_replace("\n", ' ', trim($header));
                    // trim any excess whitespace "foo   bar" => "foo bar"
                    $header = preg_replace('/(\s)+/', ' ', $header);
                    if ($isError) {
                        $this->printErrorBlock($header);
                    } else {
                        $this->printDebug($header);
                    }
                }
                $profilerUrl = $response->getHeader('X-Debug-Token-Link');
                if ($profilerUrl) {
                    $fullProfilerUrl = $response->getHeader('Host').$profilerUrl;
                    $this->printDebug('');
                    $this->printDebug(sprintf(
                        'Profiler URL: <comment>%s</comment>',
                        $fullProfilerUrl
                    ));
                }
                // an extra line for spacing
                $this->printDebug('');
            } else {
                $this->printDebug($body);
            }
        }
    }
    /**
     * Print a debugging message out in a big red block
     *
     * @param $string
     */
    protected function printErrorBlock($string)
    {
        if ($this->formatterHelper === null) {
            $this->formatterHelper = new FormatterHelper();
        }
        $output = $this->formatterHelper->formatBlock($string, 'bg=red;fg=white', true);
        $this->printDebug($output);
    }

}
