<?php


namespace AppBundle\Comunications;

use AppBundle\Entity\Comunication;
use AppBundle\Entity\ComunicationReply;
use AppBundle\Entity\User;
use AppBundle\Entity\View;
use AppBundle\Notifications\NotificationsManager;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;


class ComunicationsManager
{

    protected $em;

    private $logger;
    private $paginator;
    /**
     * @var NotificationsManager
     */
    private $notificationsManager;

    public function __construct(EntityManager $em, Paginator $paginator, NotificationsManager $notificationsManager)
    {

        $this->em = $em;
        $this->paginator = $paginator;

        $this->notificationsManager = $notificationsManager;
    }


    public function getGroupComunicates($group)
    {

        if ($this->em) {
            $comunicationsRepository = $this->em->getRepository(Comunication::class);

            $qb = $comunicationsRepository->createQueryBuilder("com");
            $qb->andWhere("com.type = 2")
                ->andWhere("com.workgroup = :group")
                ->orderBy("com.date", "DESC")
                ->setParameter("group", $group);

            return $qb->getQuery()->getResult();
        } else {
            return null;
        }

    }


    public function getGeneralComunicatesPaginator($page = 1, $limit = 10)
    {

        $comunicationsRepository = $this->em->getRepository(Comunication::class);
        $qb = $comunicationsRepository->createQueryBuilder("com");
        $qb = $qb->where("com.type = 1")->orderBy("com.date", "DESC");;


        return $this->paginator->paginate($qb->getQuery(), $page, $limit, array('pageParameterName' => 'general_com_page'));

    }

    public function getGroupComunicatesPaginator($user, $group, $page = 1, $limit = 10)
    {

        $comunicationsRepository = $this->em->getRepository(Comunication::class);
        $qb = $comunicationsRepository->createQueryBuilder("com");
        $qb->where("com.type = 2")->andWhere("com.department = :group OR com.author = :user")->setParameters(["group" => $group, "user" => $user])->orderBy("com.date", "DESC");


        return $this->paginator->paginate($qb->getQuery(), $page, $limit, array('pageParameterName' => 'group_com_page'));

    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getDirectComunicatesPaginator(User $user, $page = 1, $limit = 10)
    {


        $comunicationsRepository = $this->em->getRepository(Comunication::class);
        $qb = $comunicationsRepository->createQueryBuilder("com")->where("com.type = 3")->andWhere(":u MEMBER OF com.users OR com.author = :u")->setParameter("u", $user)->orderBy("com.date", "DESC");
        return $this->paginator->paginate($qb->getQuery(), $page, $limit, array('pageParameterName' => 'direct_com_page'));

    }


    public function getGeneralComunicates()
    {


        if ($this->em) {
            $comunicationsRepository = $this->em->getRepository(Comunication::class);

            $qb = $comunicationsRepository->createQueryBuilder("com")->orderBy("com.date", "DESC");
            $qb->where("com.type = 1");

            return $qb->getQuery()->getResult();


        } else {
            return null;
        }

    }

    public function getUnreadGeneralComunicates(User $user)
    {
        $dql = "SELECT com from AppBundle:Comunication  com WHERE com.type = 1 AND  com.id NOT IN ( SELECT IDENTITY(view.message) FROM AppBundle:View view WHERE view.user = :user) ORDER BY com.date DESC";
        $query = $this->em->createQuery($dql);
        $query->setParameter("user", $user);
        $messages = $query->getResult();
        return $messages;

    }

    public function getUnreadGroupComunicates(User $user, $group)
    {


        $dql = "SELECT com from AppBundle:Comunication  com WHERE com.type = 2 AND com.department = :group AND  com.id NOT IN ( SELECT IDENTITY(view.message) FROM AppBundle:View view WHERE view.user = :user) ORDER BY com.date DESC";
        $query = $this->em->createQuery($dql);
        $query->setParameters(["group" => $group, 'user' => $user]);

        $messages = $query->getResult();
        return $messages;

    }

    public function getUnreadDirectComunicates(User $user)
    {

        $dql = "SELECT com from AppBundle:Comunication  com WHERE com.type = 3 AND :user MEMBER OF com.users AND  com.id NOT IN ( SELECT IDENTITY(view.message) FROM AppBundle:View view WHERE view.user = :user) ORDER BY com.date DESC";
        $query = $this->em->createQuery($dql);
        $query->setParameter("user", $user);
        $messages = $query->getResult();
        return $messages;
    }

    public function save($comunicate)
    {

        $this->em->persist($comunicate);
        $this->em->flush();
        $this->notifyUsers($comunicate);

    }

    /**
     * @param Comunication $comunicate
     */
    protected function notifyUsers($comunicate)
    {
        $this->notificationsManager->notify($comunicate);

    }

    /**
     * @param $users
     * @return array $tokens
     */
    protected function tokenFromUsers($users)
    {
        $tokens = [];

        /** @var User $user */
        foreach ($users as $user) {
            if ($user->getDeviceToken() != null) {
                $tokens[] = $user->getDeviceToken();
            }
        }
        return $tokens;
    }

    /**
     * @param User $user
     * @brief Get the comunicates the user is into
     *         and the user, if it was the only user, the comunicate gets deleted as well
     *
     */
    public function removeUserFromComunicates(User $user)
    {

        $user_comunicates = $this->getDirectComunicates($user);
        //remove the user for all the comunicates
        foreach ($user_comunicates as $comunicate) {
            $comunicate->removeUser($user);
            if (count($comunicate->getUsers()) == 0) {
                //this was the only user, delete as well the comunicate
                $this->em->remove($comunicate);
            } else {
                //save it with less users
                $this->em->persist($comunicate);
            }

        }
        $comunicates = $this->getUserComunicates($user);
        if (count($comunicates) > 0) {
            foreach ($comunicates as $comunicate) {
                $this->em->remove($comunicate);
            }
        }
        $this->em->flush();


    }

    public function getDirectComunicates(User $user)
    {

        $comunicationsRepository = $this->em->getRepository(Comunication::class);
        $qb = $comunicationsRepository->createQueryBuilder("com")
            ->where("com.type = 3")
            ->andWhere(":u MEMBER OF com.users")
            ->orderBy("com.date", "DESC")
            ->setParameter("u", $user);
        $messages = $qb->getQuery()->getResult();


        return $messages;

    }

    public function getUserComunicates(User $user)
    {

        $comunicationsRepository = $this->em->getRepository(Comunication::class);
        $comunications = $comunicationsRepository->findBy(["author" => $user]);
        return $comunications;

    }

    /**
     * Register the user in the list of persons that saw the comunicate
     * @param Comunication $comunication
     * @param User $user
     */
    public function addView(Comunication $comunication, User $user)
    {
//        if ($comunication->getAuthor() == $user) return;
        $qb = $this->em->getRepository(View::class)->createQueryBuilder("view");

        $qb->where("view.user = :u")
            ->andWhere("view.message = :com")->setParameters(["u" => $user, "com" => $comunication]);
        $results = $qb->getQuery()->getResult();
        if (count($results) != 0) return;

        $view = new View();
        $view->setUser($user);
        $view->setMessage($comunication);
        $view->setViewAt(new \DateTime());
        $this->em->persist($view);
        $this->em->persist($comunication);
        $this->em->flush();
    }

    /**
     * @param $post Comunication
     * @param $reply ComunicationReply
     */
    public function saveReply($post, $reply){

      $this->em->persist($reply);
      $this->em->flush();

      $this->notificationsManager->notifyReply($post, $reply);
    }

}
