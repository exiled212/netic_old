<?php

namespace AppBundle\Settings;


use AppBundle\Entity\GlobalSettings;
use AppBundle\Form\GlobalSettingsType;
use Symfony\Component\Form\FormFactory;
use Doctrine\ORM\EntityManager;

class SettingsManager{

    protected $em;
    protected $formFactory;
    public function __construct(EntityManager $em,FormFactory $formFactory){

        $this->em = $em;
        $this->formFactory = $formFactory;


    }

    public function getSettings(){
        $repo = $this->em->getRepository(GlobalSettings::class);
        $settings = $repo->findAll();
        if(count($settings)> 0){
            return $settings[0];
        }else{
            $settings = new GlobalSettings();
            $this->em->persist($settings);
            $this->em->flush();
            return $settings;
        }

    }
    public function settingsForm(){

        return $this->formFactory->create(new GlobalSettingsType(), $this->getSettings());


    }

}