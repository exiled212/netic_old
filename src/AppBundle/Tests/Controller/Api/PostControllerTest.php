<?php

namespace AppBundle\Tests\Controller\Api;

use AppBundle\Test\ApiTestCase;

class PostControllerTest extends ApiTestCase
{


    public function testPOSTPostWorks()
    {



        for ($i = 0; $i < 25; $i++) {
            $data = ["title" => 'Test '.$i, "replies" => 1, "content" => "sample content "];
            $response = $this->client->post("/api/v2/posts", [
                'body' => json_encode($data),
                "headers" => $this->getAuthorizedHeaders("admin", [])
            ]);

            $this->assertEquals(201, $response->getStatusCode());
        }

    }

    public function testGETPostCollection()
    {

        $response = $this->client->get("/api/v2/posts",
            array('headers' =>
                $this->getAuthorizedHeaders("admin", array())
            ));
        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyIsArray($response, 'items');

    }

    public function testGETPostCollectionPaginated()
    {

        $response = $this->client->get("/api/v2/posts",
            array('headers' =>
                $this->getAuthorizedHeaders("admin", array())
            ));
        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyIsArray($response, 'items');
        $this->asserter()->assertResponsePropertyEquals($response, "count", 10);


    }

    public function testGETPostCollectionPaginatedFiltered()
    {

        $response = $this->client->get("/api/v2/posts?filter=test",
            array('headers' =>
                $this->getAuthorizedHeaders("admin", array())
            ));
        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyIsArray($response, 'items');


    }


    public function testRequireAuthentication()
    {

        $response = $this->client->post('/api/v2/posts', [
            'body' => '[]'
        ]);
        $this->assertEquals(401, $response->getStatusCode());
    }

}