<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/27/17
 * Time: 10:08 AM
 */

namespace AppBundle\Tests\Services;


use AppBundle\Entity\Comunication;
use AppBundle\Entity\User;
use AppBundle\Notifications\NotificationsManager;
use AppBundle\Test\ApiTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NotificationsManagerTest extends ApiTestCase
{


    private static $container;
    /** @var  NotificationsManager */
    private static $notManager;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        $kernel = static::createKernel();
        $kernel->boot();
        self::$container = $kernel->getContainer();

        self::$notManager = self::$container->get("one_signal_notification_manager");
    }

    public function testSendPush()
    {

//        $response = self::$notManager->sendPushToAll();
//        $this->assertEquals($response->getStatusCode(), 200);

    }
    public function  testSchedulePush(){

        $notice = new Comunication();
        $notice->setTitle("Test");
        /** @var User $author */
        $author = new User();
        $author->setFirstName("Albert");
        $author->setLastName("Einstein");
        $notice->setAuthor($author);
        $messageId = self::$notManager->schedulePushToAll($notice);

    }
    public function  testSendPushToUsers(){

        $response = self::$notManager->sendPushToUsers(56);
        $data = json_decode($response->getBody()->getContents(),true);
        $this->assertArrayHasKey("id",$data);
        $this->assertArrayHasKey("recipients", $data);
        $this->assertEquals($response->getStatusCode(), 200);

    }

    public function  testSendPushToAll(){

        $response = self::$notManager->sendPushToAll(67);
        $data = json_decode($response->getBody()->getContents(),true);
        $this->assertArrayHasKey("id",$data);
        $this->assertArrayHasKey("recipients", $data);
        $this->assertEquals($response->getStatusCode(), 200);

    }

}