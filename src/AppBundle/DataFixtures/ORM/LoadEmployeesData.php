<?php

namespace AppBundle\DataFixtures;


use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadEmployeesData implements FixtureInterface, ContainerAwareInterface
{



    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Department
     */
    protected $workgroup;

    /**
     * @var ObjectManager
     */
    protected $manager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function load(ObjectManager $manager)
    {

        $this->manager = $manager;

        $workgroup = new Department();
        $workgroup->setName('Grupo inicial');
        $workgroup->setCreatedAt(new \DateTime());
        $manager->persist($workgroup);
        $manager->flush($workgroup);

        $this->workgroup = $workgroup;


//        $this->addUser("00001s " ,  "test " , "test" , "test" , "test");


        $manager->flush();
    }


    /**
     * @param string $employee_id
     * @param string $firstName
     * @param string $lastName
     * @param string $username
     * @param string $password
     */
    public function addUser($employee_id, $firstName, $lastName, $username, $password){


        $employee_id = trim($employee_id);
        $firstName = trim($firstName);
        $lastName = trim($lastName);
        $username = trim($username);
        $password = trim($password);

        $user_manager =  $this->container->get("fos_user.user_manager");
        /** @var User $user */
        $user = $user_manager->createUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setEmployeeId($employee_id);
        $user->setPlainPassword($password);
        $user->setUsername($username);
        $user->setEmail($username."@groupnet.es");
        $user->setWorkgroup($this->workgroup);
        $user->setEnabled(true);

        $this->manager->persist($user);


    }


}
