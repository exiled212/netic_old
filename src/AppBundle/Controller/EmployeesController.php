<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use AppBundle\Form\AddressType;
use Doctrine\DBAL\Query\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class EmployeesController extends Controller
{

    const FILTER_ALL = 0;
    const FILTER_GROUP = 1;

    /**
     * @Route("/employees/list", name="employees_list")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {

        $page = $request->get("page", 1);

        //filter type
        $filter = $request->get("filter", EmployeesController::FILTER_ALL);

        $nameFilter = $request->get("table_search", "");

        $employeesRepository = $this->getDoctrine()->getRepository(User::class);

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $employeesRepository->createQueryBuilder('u');


        if (strlen($nameFilter) > 0) {
            //filter by name
            $queryBuilder->andWhere("u.firstname LIKE :name")->setParameter("name", $nameFilter);
        }


        $paginator = $this->get('knp_paginator');
        switch ($filter) {
            case EmployeesController::FILTER_ALL: {

                $employees = $paginator->paginate($queryBuilder->getQuery(), $page, 500);


                return $this->render('default/employees/list.html.twig', [
                    "employees" => $employees,
                    "name" => $nameFilter

                ]);


                break;
            }
            case EmployeesController::FILTER_GROUP: {
                $groupId = $request->get("group", -1);
                /** @var Department $group */
                $department = $this->getDoctrine()->getRepository(Department::class)->find($groupId);
                if ($department) {

                    $queryBuilder->where("u.department  = :group")->setParameter("group", $department);
                    $employees = $paginator->paginate($queryBuilder->getQuery(), $page, 10);

                    return $this->render('default/employees/list.html.twig', [
                        "employees" => $employees,
                        "department" => $department,
                        "name" => $nameFilter

                    ]);

                } else {
                    throw new  NotFoundHttpException("Invalid group id given");


                }


            }


        }


    }

    /**
     * @Route("/group/{id}/add", name="workgroup_add_user")
     */
    public function addAction($id, Request $request)
    {

        $department = $this->getDoctrine()
            ->getRepository(Department::class)->find($id);

        $users = $this->get('fos_user.user_manager')->findUsers();

        return $this->render("default/employees/add.html.twig", [
            "users" => $users,
            "department" => $department
        ]);

    }


    /**
     * @Route("/employees/show/{id}", name="employee_show")
     */
    public function showAction($id, Request $request)
    {

        // create a task and give it some dummy data for this example
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);


        if ($user) {

            list($avatar, $form) = $this->getEditUserForm($user);

            return $this->render(":default/employees:show.html.twig", ["form" => $form->createView(), "avatar" => $avatar]);
        } else {

            throw new PageNotFoundException("No valid user given");
        }

    }

    /**
     * @param $user
     * @return array
     */
    protected function getEditUserForm($user)
    {
        $avatar = $user->getAvatar();
        $user->setAvatar(new File($this->getParameter("profiles_directories") . $user->getAvatar(), false));

        $form = $this->createFormBuilder($user, array(
            'csrf_protection' => false,  // <---- set this to false on a per Form Instance basis
        ))
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add("username", TextType::class)
            ->add("phone", TextType::class)
            ->add("mobile", TextType::class)
            ->add("employee_id", TextType::class)
            ->add('avatar', FileType::class)
            ->add("birthdate", DateType::class, ["widget" => "single_text", "html5" => false, "format" => "dd/MM/y"])
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add("group_boss", CheckboxType::class, ["label" => 'leader_group', 'required' => false,])
            ->add("admin_rights", CheckboxType::class, ["label" => 'administrator', 'required' => false])
            ->add("department", 'entity', array(
                'class' => 'AppBundle\Entity\Department',
                'property' => 'name'))
            ->add("address", AddressType::class)
            ->add('save', SubmitType::class, array('label' => 'save_changes'))
            ->getForm();
        return array($avatar, $form);
    }

    /**
     * @Route("/employees/edit/{id}", name="employees_edit")
     */
    public function editAction($id, Request $request)
    {


        // create a task and give it some dummy data for this example
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);


        if ($user) {

            list($avatar, $form) = $this->getEditUserForm($user);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var User $user */
                $user = $form->getData();

                if ($user->getAdminRights()) {

                    $user->addRole("ROLE_ADMIN");

                }
                if ($user->getGroupBoss()) {
                    $user->addRole("ROLE_MANAGER");
                    $user->getDepartment()->setManager($user);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();


                $this->setFlash("success", $translated = $this->get('translator')->trans('change_success'));
                return $this->redirectToRoute("employees_list");
            }



            // }elseif($form->isSubmitted()){
            //         dump($form->getErrors());
            //         die();

            // }

            return $this->render("default/employees/edit.html.twig", ["form" => $form->createView(),  "avatar" => $avatar]);
        } else {
            throw new NotFoundHttpException("User not found");
        }


    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }

    /**
     * @Route("/employees/delete", name="employees_delete")
     */

    public function deleteAction(Request $request)
    {
        $id = $request->get("id", -1);


        if ($id == -1) {
            throw new NotFoundHttpException();
        } else {
            $em = $this->getDoctrine()->getEntityManager();
            $user = $em->getRepository(User::class)->find($id);

            if (!$user) {
                throw $this->createNotFoundException('No user  found for id ' . $id);
            } else if ($user == $this->getUser()) {
                $this->setFlash("danger", $translated = $this->get('translator')->trans('can_not_delete_own_account'));
                return $this->redirectToRoute("employees_list");
            }

            $name = $user->getFirstName();

            $this->get("users.manager")->deleteUser($user);

            $this->setFlash("success", $translated = $this->get('translator')->trans('delete_account_success'));
            return $this->redirectToRoute("employees_list");
        }
    }

    /**
     * @Route("/employees/disable", name="employee_disable")
     */
    public function disableAction(Request $request)
    {
        $id = $request->get("id", -1);


        if ($id == -1) {
            throw new NotFoundHttpException();
        } else {
            $em = $this->getDoctrine()->getEntityManager();
            $user = $em->getRepository(User::class)->find($id);

            if (!$user) {
                throw $this->createNotFoundException('No user  found for id ' . $id);
            } else if ($user == $this->getUser()) {
                $this->setFlash("warning", $translated = $this->get('translator')->trans('can_not_suspend_own_account'));
                return $this->redirectToRoute("employees_list");
            }

            $name = $user->getFirstName();

            $user->setEnabled(false);

            $this->get("users.manager")->updateUser($user);

            $this->setFlash("success", $translated = $this->get('translator')->trans('suspend_account_success'));
            return $this->redirectToRoute("employees_list");
        }
    }

    /**
     * @Route("/employees/enable", name="employee_enable")
     */

    public function enableAction(Request $request)
    {
        $id = $request->get("id", -1);


        if ($id == -1) {
            throw new NotFoundHttpException();
        } else {
            $em = $this->getDoctrine()->getEntityManager();
            $user = $em->getRepository(User::class)->find($id);

            if (!$user) {
                throw $this->createNotFoundException('No user  found for id ' . $id);
            } else if ($user == $this->getUser()) {
                $this->setFlash("warning", $translated = $this->get('translator')->trans('can_not_suspend_own_account'));
                return $this->redirectToRoute("employees_list");
            }

            $name = $user->getFirstName();

            $user->setEnabled(true);

            $this->get("users.manager")->updateUser($user);

            $this->setFlash("success", $translated = $this->get('translator')->trans('activate_account_success'));
            return $this->redirectToRoute("employees_list");
        }
    }

    /**
     * @param $employee
     * Check if the new created employee is a valid one
     * WARNING: side effect, set flash bag with error
     */
    function validEmployee($employee)
    {
        $usersRepository = $this->getDoctrine()->getRepository(User::class);
        $existingUser = $usersRepository->findOneBy(["username" => $employee->getUserName()]);
        if ($existingUser != null) {
            $this->setFlash("danger", $translated = $this->get('translator')->trans('user_already_exists'));
            return false;

        }

    }

    protected function handleAvatar(User $user)
    {

        $file = $user->getAvatar();
        if ($file) {
            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                "assets/images/profiles",
                $fileName
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $user->setAvatar($fileName);
        } else {

            $user->setAvatar("default.png");
        }
    }
}
