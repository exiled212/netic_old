<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Department;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;


class WorkgroupsController extends Controller
{
    /**
     * @Route("/workgroups/list", name="workgroups_list")
     */
    public function listAction()
    {

        /** @var EntityRepository $wgRepository */
        $wgRepository = $this->getDoctrine()->getRepository(Department::class);

        $workgroups = $wgRepository->findAll();
        return $this->render("default/workgroups/list.html.twig", [
            "workgroups" => $workgroups
        ]);
    }

    /**
     * @Route("/workgroups/create", name="workgroups_create")
     */
    public function createAction(Request $request)
    {


        // create a task and give it some dummy data for this example
        $workgroup = new Department();

        $form = $this->createFormBuilder($workgroup)
            ->add("name", TextType::class, ["label" =>  'workgroup_name'])
            ->add('save', SubmitType::class, array('label' => 'create_workgroup'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated

            $workgroup = $form->getData();
            $workgroup->setCreatedAt(new \DateTime());
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $em = $this->getDoctrine()->getManager();
            $em->persist($workgroup);
            $em->flush();
            $this->setFlash('success',  $translated = $this->get('translator')->trans('create_workgroup_success'));
            return $this->redirectToRoute('workgroups_list');
        }

        return $this->render("default/workgroups/create.html.twig", [
            "form" => $form->createView()
        ]);


    }

    /**
     * @param Request $request
     * @Route("/workgroups/delete", name="workgroup_delete"))
     */
    public function deleteAction(Request $request)
    {

        $id = $request->get("id", -1);


        if ($id == -1) {
            throw new NotFoundHttpException();
        } else {
            $em = $this->getDoctrine()->getEntityManager();
            $wg = $em->getRepository(Department::class)->find($id);

            if (!$wg) {
                throw $this->createNotFoundException('No workgroup  found for id ' . $id);
            } else if (count($wg->getMembers()) == 0) {
                $name = $wg->getName();

                $em->remove($wg);
                $em->flush();
                $this->setFlash("success", $translated = $this->get('translator')->trans('workgroup_successfully_eliminated'));
                return $this->redirectToRoute("workgroups_list");

            } else {
                //el grupo tiene miembros, no puede eliminarse
                $this->setFlash("danger", $translated = $this->get('translator')->trans('can_not_delete_workgroup_contains_employees'));
                return $this->redirectToRoute("workgroups_list");
            }

        }

    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }
}