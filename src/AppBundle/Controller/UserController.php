<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comunication;
use AppBundle\Entity\Locale;
use AppBundle\Entity\Department;
use AppBundle\Form\AddressType;
use GuzzleHttp\Psr7\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\User;

class UserController extends Controller
{

    /**
     * @Route("/user/profile", name = "user_profile")
     */
    public function showProfileAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();
        $username = $this->getUser()->getUserName();
        $workgroup = $user->getDepartment();
        $employee_id = $user->getEmployeeId();
//        dump($user->getAvatar());

        $avatar = $user->getAvatar();


        if ($user->getBirthdate()) {
            //convert to string to show in the field
            $user->setBirthdate($user->getBirthdate()->format("d/m/y"));
        }


        $form = $this->createFormBuilder($user, array(
            'csrf_protection' => false,  // <---- set this to false on a per Form Instance basis
        ))
            ->add("username",TextType::class)
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add("phone", TextType::class)
            ->add("mobile", TextType::class)
            ->add("employee_id", TextType::class)
            ->add("avatar", FileType::class, ["mapped" => false])
            ->add("birthdate", TextType::class)
//            ->add('plainPassword', 'repeated', array(
//                'type' => 'password',
//                'options' => array('translation_domain' => 'FOSUserBundle'),
//                'first_options' => array('label' => 'form.password'),
//                'second_options' => array('label' => 'form.password_confirmation'),
//                'invalid_message' => 'fos_user.password.mismatch',
//            ))
            ->add("workgroup", TextType::class, ["data" => $user->getDepartment()->getName(), "mapped" => false])
            ->add("address", AddressType::class)
            ->add("email", EmailType::class)
            ->add('save', SubmitType::class, array('label' => 'save_changes'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            //access the form unmaped data
            /** @var UploadedFile $uploadedAvatar */
            $uploadedAvatar = $form['avatar']->getData();

            if($uploadedAvatar){
                $user->setAvatar($uploadedAvatar);
            }
            if ($user->getBirthdate()) {
                //a birthdate was provided, the field is not null
                $date_array = date_parse($user->getBirthdate());

                $date = new \DateTime();
                $date->setDate($date_array["year"], $date_array["month"], $date_array["day"]);
                $user->setBirthdate($date);
            }
            $user->setUserName($username);


            $user->setEmployeeId($employee_id);
            $user->setDepartment($workgroup);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute("homepage");

        }

        return $this->render("default/user_profile.html.twig", ["form" => $form->createView(), "avatar" => $avatar]);

    }


}
