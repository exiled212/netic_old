<?php

namespace AppBundle\Controller\Api;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class TokenController extends Controller
{
    /**
     * @Route("/auth/token")
     * @Method("POST")
     */
    public function newTokenAction(Request $request)
    {
        $user = $this->get("fos_user.user_manager")->findUserByUsername($request->getUser());
        if (!$user) {
            throw new NotFoundHttpException();
        }
        $isValid = $this->get("security.password_encoder")->isPasswordValid($user, $request->getPassword());

        if (!$isValid) {
            throw new BadCredentialsException();
        }
        $durationInDays = 10*3600*24;

        $token = $this->get("lexik_jwt_authentication.encoder")
            ->encode([
                "username" => $user->getUsername(),
                "exp" => time() + $durationInDays
            ]);

        return new JsonResponse(["token" => $token]);

    }

}