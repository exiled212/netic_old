<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/10/17
 * Time: 11:06 AM
 */

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Address;
use AppBundle\Entity\Attachment;
use AppBundle\Entity\Comunication;
use AppBundle\Entity\ComunicationReply;
use AppBundle\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends  Controller
{

    public function serializePost(Comunication $message)
    {

        return [
            "id" => $message->getId(),
            "title" => $message->getTitle(),
            "createdAt" => $message->getDate(),
            "content" => $message->getContent(),
            "author" => $this->getUserArrayData($message->getAuthor()),
            "acceptsReplies" => $message->getAcceptsReplies(),
            "type" => $message->getType(),
            "views" => $this->serializeMessageViews($message->getViews()),
            "attachments" => $this->serializeAttachements($message->getAttachments()),
            "replies" => $this->serializeReplies($message->getReplies()),
        ];
    }

    public function getUserArrayData(User $user)
    {

        $user_arr = [
            "id" => $user->getId(),
            "firstName" => $user->getFirstName(),
            "lastName" => $user->getLastName(),
            "profilePicUrl" => $this->getParameter("profiles_rel_directories") . $user->getAvatar()
        ];

        return $user_arr;
    }

    private function serializeMessageViews($views)
    {
        $arr = [];
        if ($views) {
            /** @var View $view */
            foreach ($views as $view) {
                $arr [] = ["user" => $this->serializeUser($view->getUser()), "date" => $view->getViewAt()];
            }
        }
        return $arr;
    }

    protected function serializeUser(User $user)
    {

        $group = $user->getWorkgroup();

        $user_arr = [
            "id" => $user->getId(),
            "firstName" => $user->getFirstName(),
            "lastName" => $user->getLastName(),
            "username" => $user->getUsernameCanonical(),

            "group" => [
                "id" => $group->getId(),
                "name" => $group->getName()
            ],
            "admin" => $user->getAdminRights(),
            "manager" => $user->getGroupBoss(),
            "email" => $user->getEmail()
        ];
        $user_arr["permissions"] = $this->serializePermissions();
        if ($user->getAvatar()) {
            //the user has an avatar

            $user_arr["profilePicUrl"] = $this->getParameter("profiles_rel_directories") . $user->getAvatar();
        } else {

            $user_arr["profilePicUrl"] = "assets/img/dummy.png";
        }

        if ($user->getAddress()) {
            $user_arr["address"] = $this->serializeAddress($user->getAddress());

        }
        return $user_arr;

    }

    protected function serializePermissions()
    {
        $perm_arr = [];
        $settings = $this->get("settings")->getSettings();
        $perm_arr["employee_messaging"] = $settings->getEmployeeMessaging();
        $perm_arr["admin_messaging"] = $settings->getAdminMessaging();
        $perm_arr["manager_to_group_messaging"] = $settings->getManagerToGroupMessaging();
        $perm_arr["manager_direct_messages"] = $settings->getManagerDirectMessages();
        return $perm_arr;
    }

    private function serializeAddress(Address $address)
    {
        if ($address) {
            return
                ["line1" => $address->getLine1(),
                    "region" => $address->getRegion(),
                    "city" => $address->getCity(),
                    "country" => $address->getCountry(),
                    "zip_code" => $address->getZipCode()
                ];
        } else {
            return null;
        }

    }

    private function serializeAttachements($attachmentsList)
    {
        $arr = [];

        if ($attachmentsList) {

            /** @var Attachment $attachment */
            foreach ($attachmentsList as $attachment) {
                $arr [] = ['id' => $attachment->getId(), 'path' => $attachment->getFilename(), "name" => $attachment->getSourceName()];
            }
        }
        return $arr;
    }

    protected  function serializeReplies($replies)
    {
        $repliesArr = [];
        if ($replies) {

            /** @var ComunicationReply $reply */
            foreach ($replies as $reply) {
                $repliesArr [] = $this->serializeReply($reply);

            }
        }
        return $repliesArr;
    }

    protected function serializeReply(ComunicationReply $reply){

        return ["id" => $reply->getId(),
            "author" => $this->serializeUser($reply->getAuthor()),
            "content" => $reply->getContent(),
            "createdAt" => $reply->getCreatedAt()];
    }
    public function createApiResponse($data, $statusCode = 200){


        $json = $this->serialize($data);

        return  new Response($json,$statusCode,array( "Content-Type"=>"application/json"));

    }
    private function serialize($data)
    {
        return $this->get('jms_serializer')
            ->serialize($data, 'json');
    }

    protected function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }

    protected function createValidationErrorResponse(FormInterface $form)
    {
        $errors = $this->getErrorsFromForm($form);
        $data = [
            'type' => 'validation_error',
            'title' => 'There was a validation error',
            'errors' => $errors
        ];
        $response =  new JsonResponse($data, 400);
        $response->headers->set('Content-Type', 'application/problem+json');
        return $response;
    }
}