<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/10/17
 * Time: 10:56 AM
 */

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Comunication;
use AppBundle\Entity\ComunicationReply;
use AppBundle\Form\ReplyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RepliesController extends ApiController
{
    /**
     * Add a new reply to a post
     * @Route("/posts/{id}/replies")
     * @Method("POST")
     */
    public function newAction($id, Request $request)
    {

        /** @var Comunication $post */
        $post = $this->get("doctrine")->getRepository('AppBundle:Comunication')->find($id);

        $data = $data = json_decode($request->getContent(), true);
        $reply = new ComunicationReply();
        $form = $this->createForm(new ReplyType(), $reply);
        $form->submit($data);
        $reply->setAuthor($this->getUser());
        $reply->setComunication($post);
        $reply->setHasBeenSeen(false);
        $reply->setCreatedAt(new \DateTime());
        $post->addReply($reply);

        //save the reply
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->persist($reply);
        $em->flush();

        $response = $this->createApiResponse($reply, 201);

        $url = $this->generateUrl("api_v2_reply_show", ["id" => $reply->getId()]);
        $response->headers->set("Location", $url);
        return $response;
    }

    /**
     * @Route("/replies/{id}",name="api_v2_reply_show")
     */
    public function showAction($id)
    {

        $reply = $this->getDoctrine()->getRepository("AppBundle:ComunicationReply")->find($id);
        return new JsonResponse($this->serializeReply($reply));

    }

    /**
     * @param $id
     * @Route("/posts/{id}/replies")
     * @Method("GET")
     */
    public function listAction($id)
    {

        $post = $this->getDoctrine()->getRepository("AppBundle:Comunication")->find($id);
        $replies = $post->getReplies();
        $data = ["replies" => $replies];
        return $this->createApiResponse($data);

    }


}