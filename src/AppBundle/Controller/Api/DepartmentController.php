<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Department;
use AppBundle\Form\DepartmentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DepartmentController extends ApiController
{

    /**
     * @Route("/departments",name = "api_v2_departments_list")
     * @Method("GET")
     */
    public function listAction(Request $request)
    {

        $page = $request->query->getInt("page", 1);
        $qb = $this->getDoctrine()->getRepository("AppBundle:Department")->findAllQueryBuilder();

        $route = "api_v2_departments_list";
        $paginatedCollection = $this->get("pagination_factory")->createCollection($qb, $request, $route);
        return $this->createApiResponse($paginatedCollection);
    }


    /**
     * @Route("/departments")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $department = new Department();

        $form = $this->createForm(new DepartmentType(), $department);
        $form->submit($data);

        $department->setCreatedAt(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($department);
        $em->flush();



        $response = $this->createApiResponse($department, 201);

        $url = $this->generateUrl("api_v2_workgroup_show", ["id" => $department->getId()]);
        $request->headers->set("Location", $url);

        return $response;
    }

    /**
     * @Route("/workgroups/{id}")
     * @Method("POST")
     */
    public function updateWorkgroupAction($id)
    {

    }

    /**
     * @Route("/workgroups/{id}",name="api_v2_workgroup_show")
     * @Method("GET")
     */
    public function getWorkgroupAction($id)
    {

    }

}
