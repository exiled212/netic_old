<?php

namespace AppBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class RegistrationType extends AbstractType
{

    private $profilesDirectory;

    public function __construct($profilesDirectory)
    {
        $this->profilesDirectory = $profilesDirectory;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle', 'required' => false))
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('firstname', TextType::class, ["required" => true])
            ->add('lastname', TextType::class, ["required" => true])
            ->add("phone", TextType::class, ["required" => false])
            ->add("mobile", TextType::class, ["required" => false])
            ->add("employee_id", TextType::class, ["required" => true])
            ->add('avatar', FileType::class, ["required" => false])
            ->add("birthdate", DateType::class, ["widget" => "single_text", "html5" => false, "format" => "dd/MM/y"])
            ->add("group_boss", CheckboxType::class, ["label" => " Marcar si es jefe de grupo", 'required' => false,])
            ->add("admin_rights", CheckboxType::class, ["label" => "Conceder permisos de adminstrador", 'required' => false])
            ->add("department", 'entity', array(
                'class' => 'AppBundle\Entity\Department',
                'property' => 'name'))
            ->add("address", AddressType::class, ["required" => false]);


    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }


    public function getName()
    {
        return 'bundle_registration_type';
    }
}
