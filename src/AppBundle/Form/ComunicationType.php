<?php

namespace AppBundle\Form;

use AppBundle\Entity\Address;
use AppBundle\Entity\Comunication;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ComunicationType extends AbstractType
{

//
//    private $session;
//
//    public function __construct($session, $class)
//    {
//        $this->session = $session;
//        parent::__construct($class);
//    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $locale = $options["locale"];


        $toolbar = array(
            array(
                "name" => 'styles',
                "items" => [ 'Styles','Format','Font','FontSize' ]
            ),
            array(
                "name" => 'editing',
                "items" => [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ]
            ),
            array(
                "name" => 'clipboard',
                "items" => [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ]
            ),
            array(
                "name" => 'tools',
                "items" => [ 'Maximize', 'ShowBlocks' ]
            ),
            array(
                'name'  => 'basicstyles',
                'items' => array('Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat'),

            ),
            array(
                'name'  => 'paragraph',
                'items' => [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
                    '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ]
            ),
            array(
                'name'  => 'links',
                'items' => array('Link','Unlink'),
            ),
            array(
                "name" => 'insert',
                "items" => [ 'Image','addImage','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ]
            )

        );
        $ckconfig = array(
            "extraPlugins" => 'image2',
            'toolbar' => $toolbar,
            'forcePasteAsPlainText'=> true,
            'input_sync' => true,
            'language' => $locale,
            'filebrowserBrowseUrl' => '/api/browser/browse/type/all',
            'filebrowserUploadUrl' => '/api/browser/upload/type/all',
            'filebrowserImageBrowseUrl' => '/api/browser/browse/type/image',
            'filebrowserImageUploadUrl' => '/api/browser/upload/type/image',
        );

        $builder->add('title', TextType::class)
            ->add("content", CKEditorType::class,[
                "config"=>$ckconfig,
                'base_path' => 'assets/vendor/ckeditor',
                'js_path'   => 'assets/vendor/ckeditor/ckeditor.js',
            ])
            ->add("attachments",CollectionType::class, [
                "required"=>"false",
                "allow_add"=>true,
                'allow_delete' => true,
                "entry_type" => AttachmentType::class,
                'by_reference' => false
            ])
            ->add("save",SubmitType::class, array("label" => "send"));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Comunication::class,
            'locale'=> []
        ));
    }


}
