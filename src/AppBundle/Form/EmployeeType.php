<?php

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("username","text")
            ->add("firstname", "text")
            ->add("lastname", "text")
            ->add("emailCanonical", "email")
            ->add("phone", "text")
            ->add("mobile", "text")
            ->add("birthdate", "date",['widget' => 'single_text'])
            ->add("plainPassword","text")
            ->add("groupBoss", CheckboxType::class, ["required" => false])
            ->add("adminRights", CheckboxType::class, ["required" => false])
            ->add("address", AddressType::class, ["required" => false]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => "AppBundle\Entity\User",
        ]);
    }

    public function getName()
    {
        return "Employee";
    }


}
