<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/10/17
 * Time: 9:26 AM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("title", "text", ["required" => true])
            ->add("content", "textarea", ["required" => true])
            ->add("type", "number", ["required" => true])
            ->add("acceptsReplies", CheckboxType::class, ['required' => false]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => "AppBundle\Entity\Comunication",
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return "post";
    }


}
