<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/10/17
 * Time: 8:28 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UserRepository extends  EntityRepository
{

    public function findAllQueryBuilder($filter = '', $type = null, $unread = false)
    {
        $qb = $this->createQueryBuilder("user");
        if($filter){
            $qb->andWhere("user.firstname LIKE :filter")->setParameter("filter",'%'.$filter.'%');
            $qb->orderBy("user.lastname",'ASC');
        }

        return $qb;
    }

}