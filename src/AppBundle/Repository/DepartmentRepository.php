<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/10/17
 * Time: 11:54 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DepartmentRepository extends EntityRepository
{

    public function findAllQueryBuilder(){
        $qb = $this->createQueryBuilder("department");
        return $qb;
    }
}