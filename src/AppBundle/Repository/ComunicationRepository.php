<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/10/17
 * Time: 1:47 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use Doctrine\ORM\EntityRepository;

class ComunicationRepository extends EntityRepository
{
    public function findAllQueryBuilder($options, User $user = null)
    {
        $qb = $this->createQueryBuilder("post");
        $qb->andWhere("post.type = 1");

        if (array_key_exists("filter",$options)) {
            $filter = $options["filter"];
            $qb->andWhere("post.title LIKE :filter")->setParameter("filter", '%' . $filter . '%');

        }
        if($options["unread"]){
            assert($user != null);
            $qb->andWhere('post.id NOT IN (SELECT IDENTITY(view.message) FROM AppBundle:View view WHERE view.user = :user)')
                ->setParameter("user",$user);
        }

        $qb->orderBy("post.date", 'DESC');
        return $qb;
    }


    public function findInDepartmentQueryBuilder(Department $deparment, array $options, User $user = null)
    {

        $qb = $this->createQueryBuilder("com");
        $qb->andWhere("com.type = 2");
        $qb->andWhere("com.department = :department")
            ->setParameter("department", $deparment);

        if(array_key_exists("filter", $options)){
            $filter = $options["filter"];
            $qb->andWhere("com.title LIKE :filter")->setParameter("filter", '%' . $filter . '%');
        }
        if ($options["unread"]) {
            assert($user != null);

            $qb->andWhere("com.id NOT IN (SELECT IDENTITY(view.message) FROM AppBundle:View view WHERE view.user = :user)")->setParameter("user", $user);
        }

        $qb->orderBy("com.date", "DESC");

        return $qb;
    }


    public function findDirectQueryBuilder(User $user, array $options)
    {

        $qb = $this->createQueryBuilder("com");
        $qb->andWhere("com.type = 3");
        if(array_key_exists("filter", $options)){
            $filter = $options["filter"];
            $qb->andWhere("com.title LIKE :filter")->setParameter("filter", '%' . $filter . '%');
        }
        if ($options["unread"] == false) {
            $qb->andWhere(":u MEMBER OF com.users OR com.author = :u")->setParameter("u", $user);

        } else {
            // get only unread meassages
            // that where send to that user
            $qb->andWhere(":user MEMBER OF com.users")
                ->andWhere("com.id NOT IN (SELECT IDENTITY(view.message) FROM AppBundle:View view WHERE view.user = :user)");
            $qb->setParameter("user", $user);
        }

        $qb->orderBy("com.date", "ASC");

        return $qb;
    }


}