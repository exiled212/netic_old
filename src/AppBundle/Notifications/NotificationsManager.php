<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/11/17
 * Time: 3:37 PM
 */

namespace AppBundle\Notifications;


use AppBundle\Entity\Comunication;
use AppBundle\Entity\ComunicationReply;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Monolog\Logger;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Uecode\Bundle\QPushBundle\Event\MessageEvent;
use Uecode\Bundle\QPushBundle\Provider\ProviderInterface;


class NotificationsManager
{
    const API_TOKEN = "NzNiMzkwYjYtZDRkMC00YTViLWIzOTItOTE2ZjRhNjliMGY2";
    const NOTICATION_TYPE_NOTICE = 1;
    const NOTIFICATION_TYPE_GROUP_MESSAGE = 2;
    const NOTIFICATION_TYPE_PRIVATE_MESSAGE = 3;
    const NOTIFICATION_TYPE_MESSAGE_REPLY = 4;

    private $logger;
    private $client;

    // type of notifications,
    // will be extended later on
    private $em;
    /**
     * @var ProviderInterface
     */
    private $provider;
    private $appId;

    public function __construct(Logger $logger, ProviderInterface $provider, Client $client, EntityManager $em)
    {

        $this->logger = $logger;
        $this->provider = $provider;
        $this->client = $client;
        $this->em = $em;

        $this->appId = "0a657cd1-06a7-4691-9abe-70ea6354a3a4"; //one signal app id

    }

    public function onMessageReceived(MessageEvent $event)
    {

        $this->logger->critical("Message received");
        $queue_name = $event->getQueueName();
        $message = $event->getMessage();
        $body = $message->getBody();

        foreach (array_keys($body) as $key) {
            $this->logger->critical("key => " . $key);
        }
        $type = $body["type"];
        $postId = $body["id"];


        switch ($type) {
            //general comunicate
            case NotificationsManager::NOTICATION_TYPE_NOTICE:
                return $this->sendPushToAll($postId);
                break;

            case NotificationsManager::NOTIFICATION_TYPE_GROUP_MESSAGE:
                // group comunicate
                return $this->sendPushToDepartment($postId);
                break;

            case NotificationsManager::NOTIFICATION_TYPE_PRIVATE_MESSAGE:
                // private message
                return $this->sendPushToUsers($postId);
                break;
            case NotificationsManager::NOTIFICATION_TYPE_MESSAGE_REPLY:
                $replyId = $body["reply"];
                return $this->sendPushForReply($postId, $replyId);
                break;
            default:
                return null;
        }
    }

    public function sendPushToAll($id)
    {
        // fetch the post from the database
        $post = $this->getPost($id);

        if ($post) {

            $data = ["app_id" => $this->appId,
                "included_segments" => ["All"],
                "contents" => [
                    "en" => $post->getTitle(),
                    "es" => $post->getTitle()
                ]
            ];

            return $this->sendNotification($data);
        }
        return null;
    }

    /**
     * @param $id integer
     * @return Comunication|null|object
     */
    protected
    function getPost($id)
    {
        $this->logger->critical("post with id " . $id);
        $post = $this->em->getRepository('AppBundle:Comunication')->find($id);
        if ($post != null) {
            $this->logger->critical("post found");
        }
        return $post;
    }

      /**
     * @param $id integer
     * @return ComunicationReply|null
     */
    protected
    function getReply($id)
    {
        $this->logger->critical("post with id " . $id);
        $reply = $this->em->getRepository('AppBundle:Reply')->find($id);
        if ($reply != null) {
            $this->logger->critical("reply found");
        }
        return $reply;
    }

// send a push to all users
// the post type should correspond to a direct message

    /**
     * @param $data
     */
    protected
    function sendNotification($data)
    {
        $response = $this->client->post("api/v1/notifications", [
            "headers" => ["Authorization" => "Basic " . NotificationsManager::API_TOKEN,
                "Content-Type" => "application/json"],
            "body" => json_encode($data)
        ]);
        return $response;
    }

    public
    function sendPushToDepartment($postId)
    {
        $post = $this->getPost($postId);
        if ($post) {
            assert($post->getType() == Comunication::TYPE_GROUP);
            $departmentId = $post->getWorkgroup()->getId();
            $filters = [];
            // filter for departmentId
            $filters [] = [
                "field" => "tag",
                "key" => "departmentId",
                "relation" => "=",
                "value" => $departmentId
            ];

            $data = ["app_id" => $this->appId,
                "filters" => $filters,
                "contents" => [
                    "en" => $post->getTitle(),
                    "es" => $post->getTitle()
                ]
            ];

            return $this->sendNotification($data);
        }

        return null;
    }

    public
    function sendPushToUsers($postId)
    {
        $post = $this->getPost($postId);

        if ($post) {
            assert($post->getType() == Comunication::TYPE_DIRECT);
            $users = $post->getUsers();
            $tag = "userId";
            $filters = [];
            /** @var User $user */

            for ($i = 0; $i < count($users); $i++) {
                $user = $users[$i];
                if ($i) {
                    $filters[] = ["operator", "OR"];
                }
                $filters[] = [
                    "field" => "tag",
                    "key" => "userId",
                    "relation" => "=",
                    "value" => $user->getId()
                ];
            }
            $data = ["app_id" => $this->appId,
                "filters" => $filters,
                "contents" => [
                    "en" => $post->getTitle(),
                    "es" => $post->getTitle()
                ]
            ];
            return $this->sendNotification($data);
        }
        throw new NotFoundResourceException("post does not exists");
    }

    public
    function notify(Comunication $post)
    {
        $this->logger->critical("scheduling push");
        $data = ["type" => $post->getType(), 'id' => $post->getId()];
        return $this->provider->publish($data);
    }


    /**
     * @param Comunication $comunication
     * @param $reply
     * @return mixed
     */
    public function notifyReply(Comunication $comunication, ComunicationReply $reply){
        $data = ["type" => NotificationsManager::NOTIFICATION_TYPE_MESSAGE_REPLY,
        "id" => $comunication->getId(),"reply"=> $reply->getId()];
        return  $this->provider->publish($data);
    }

    /**
     * Connects to the rest api and send the push
     */
    public function sendPushForReply($postId, $replyId){
        $post = $this->getPost($postId);
        $reply = $this->getReply($replyId);

        $userId = $post->getAuthor()->getId();
        $replyUser = $reply->getAuthor();

        $userFilter   = [
            "field" => "tag",
            "key" => "userId",
            "relation" => "=",
            "value" => $userId
        ];

        $esMsg = $replyUser->getFirstname()." respondió a tu mensaje";
        $enMsg = $replyUser->getFirstname()." replied to your messsage";

        $data = ["app_id" => $this->appId,
                "filters" => [$userFilter],
                "contents" => [
                    "en" => $enMsg,
                    "es" => $esMsg
                ]];
        return  $this->sendNotification($data);
    }


}
