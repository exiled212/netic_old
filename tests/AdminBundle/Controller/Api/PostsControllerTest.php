<?php

use AppBundle\Controller\Api\PostsController;


class PostsControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testPOST(){
        $client = new \GuzzleHttp\Client([
            'base_url' => 'http://127.0.0.1:8000',
            'defautls' => [
                'exceptions' => false
            ]
        ]);

        $client->post("/api/v2/posts",[
                'body' => json_encode(["title"=>"post1"])
        ]);

    }

}
