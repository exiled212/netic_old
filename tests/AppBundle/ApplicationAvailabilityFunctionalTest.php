<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationAvailabilityFunctionalTest extends WebTestCase
{


    /**
     * @dataProvider  urlProvider
     */
    public function testPageIsSuccesfull($url)
    {

        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'admin']);
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {

        return [
            array("/login"),
            array("/es/"),
            array("/api/user/list"),
            array("/api/group/list")
        ];
    }

}
