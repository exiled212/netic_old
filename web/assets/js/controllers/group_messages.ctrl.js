

sidebarApp.controller("GroupMessagesSidebarCtrl", ["$http", "$location", "$scope", function ($http, $location, $scope) {


    var API_URL = "/api/v2/posts?type=2&department=" + appUserDeparmentId;

    this.posts = [];
    var self = this;
    var path = window.location.pathname;
    var locale = path.split("/")[1];
    console.log(locale);
    self.loadingMore = false;
    self.searching = false;
    var loadPosts = function (url) {
        self.loadingMore = true;
        return $http({
            method: "GET",
            url: url
        }).then(function (response) {
            var data = response.data;

            //preprocess the data
            var posts = response.data.items.map(function (item) {
                item.url = Routing.generate('comunications_show', {id: item.id, _locale: locale});
                //set to outbound if the message was sent by the current user
                item.outbound = (item.author.id == appUserId);
                return item;
            });

            self.currentPage = data._links.self;
            self.nextPage = data._links.next;
            self.loadingMore = false;
            return posts;
        }, function (failure) {
            self.loadingMore = false;
        });

    };


    loadPosts(API_URL + "&page=1").then(function (posts) {
        self.posts = posts;
    });


    self.loadMore = function () {
        console.log("load more");
        if (!self.loadingMore) {
            if (self.currentPage && self.nextPage) {
                loadPosts(self.nextPage).then(function (newPosts) {
                    self.posts = self.posts.concat(newPosts);
                })
            }
        }
    };
    $scope.$watch("ctrl.filter", function (newValue, oldValue) {
        if(newValue != undefined) {
            self.searching = true;
            var searchUrl = API_URL + "&page=1&filter=" + newValue;
            console.log(searchUrl);
            loadPosts(searchUrl).then(
                function (posts) {
                    self.posts = posts;
                    self.searching = false;
                }
            );
        }

    });
    self.getSearchFieldClass = function(){
        return {'input-search-container': true,'searching': self.searching }
    };

}]);