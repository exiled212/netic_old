/**
 * Sidebar app for handling the messages loading
 * @author: Miguel Carvajal
 * @email: mcarvajal@inbox.ru
 */

var sidebarApp = angular.module("SidebarApp", ["infinite-scroll"]);

sidebarApp.config(function ($interpolateProvider) {
	// for using side by side with twig template engine 
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
});